
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `account` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', '管理员', 'admin', 'admin');

-- ----------------------------
-- Table structure for choice
-- ----------------------------
DROP TABLE IF EXISTS `choice`;
CREATE TABLE `choice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `q_id` int(11) DEFAULT NULL,
  `moption` varchar(255) DEFAULT NULL,
  `ch` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of choice
-- ----------------------------
INSERT INTO `choice` VALUES ('33', '14', 'A.0-5次', '');
INSERT INTO `choice` VALUES ('34', '14', 'B.6-10次', '');
INSERT INTO `choice` VALUES ('35', '14', 'C.从来不买', '');
INSERT INTO `choice` VALUES ('36', '14', 'D.有钱任性，想买就买', '');
INSERT INTO `choice` VALUES ('37', '15', 'A.0-3次', '');
INSERT INTO `choice` VALUES ('38', '15', 'B.4-10次', '');
INSERT INTO `choice` VALUES ('39', '15', 'C.从来不', '');
INSERT INTO `choice` VALUES ('40', '15', 'D.有钱任性，数不过来多少次', '');
INSERT INTO `choice` VALUES ('43', '14', '100', '');

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` varchar(255) DEFAULT NULL,
  `goods_title` varchar(255) DEFAULT NULL,
  `goods_desc` varchar(255) DEFAULT NULL,
  `goods_img` varchar(255) DEFAULT NULL,
  `goods_price` double(10,2) DEFAULT NULL,
  `publisher_id` varchar(255) DEFAULT NULL,
  `publish_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES ('11', 'JcALgfD3O1anEu8J', '华为荣耀10', '荣耀10，渐变色机身。采用了变色镀膜工艺 [4]  ，利不同波长光在折射时偏转角度不同，控制光线在每一层镀膜上的传播路径。当光线照射在荣耀10的玻璃背壳时，穿过层层的纳米级波浪纹理膜片，光束将产生奇妙的衍射干涉和折射，最终得到丰富的、无穷变化的颜色光', '05.jpg', '2390.00', 'admin', '2020-05-13 21:12:04');
INSERT INTO `goods` VALUES ('18', 'b33MQ1H0KVh8u3U2', '华为p30', '华为P30，是华为公司旗下一款手机。手机搭载海思Kirin 980处理器，屏幕为6.1英寸，分辨率2340*1080像素。 [1]  摄像头最大30倍数码变焦。', '06.jpg', '3500.00', 'admin', '2020-05-13 21:18:07');

-- ----------------------------
-- Table structure for goodspay
-- ----------------------------
DROP TABLE IF EXISTS `goodspay`;
CREATE TABLE `goodspay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of goodspay
-- ----------------------------
INSERT INTO `goodspay` VALUES ('16', 'JcALgfD3O1anEu8J', 'NLAHQNt4607V594q');
INSERT INTO `goodspay` VALUES ('17', 'lVosfcdEkDSAv0Aa', '7MD1OoNHcT3l10au');
INSERT INTO `goodspay` VALUES ('18', 'JcALgfD3O1anEu8J', 'Hdp8x0824y1A8U0O');
INSERT INTO `goodspay` VALUES ('19', 'JcALgfD3O1anEu8J', '3CXP4pLhqe6W3yy4');
INSERT INTO `goodspay` VALUES ('20', 'JcALgfD3O1anEu8J', 'APdA59ufH053qPTa');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `avatarUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('25', '2020-04-22 21:10:19', 'Alien', '社区免费体检通知.xml', '1.jpg', '社区免费体检通知', '01.jpg');
INSERT INTO `message` VALUES ('26', '2020-04-22 21:14:01', 'Muce', '社区养生讲座通知.xml', '5.jpg', '社区养生讲座通知', '03.png');
INSERT INTO `message` VALUES ('27', '2020-04-22 21:19:25', 'Muse', '停车场管理系统升级改造的通知.xml', '3.jpg', '停车场管理系统升级改造的通知', '03.png');

-- ----------------------------
-- Table structure for pay
-- ----------------------------
DROP TABLE IF EXISTS `pay`;
CREATE TABLE `pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `itemName` varchar(255) DEFAULT NULL,
  `payment` double(255,0) DEFAULT NULL,
  `paytime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of pay
-- ----------------------------
INSERT INTO `pay` VALUES ('14', 'NLAHQNt4607V594q', '电费', '111', '2020-03-20 23:40:00');
INSERT INTO `pay` VALUES ('18', 'APdA59ufH053qPTa', '物业费', '200', '2020-04-25 12:08:23');

-- ----------------------------
-- Table structure for question
-- ----------------------------
DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of question
-- ----------------------------
INSERT INTO `question` VALUES ('14', '一年买多少次衣服？');
INSERT INTO `question` VALUES ('15', '一年出国几次旅游');

-- ----------------------------
-- Table structure for repair
-- ----------------------------
DROP TABLE IF EXISTS `repair`;
CREATE TABLE `repair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `my_content` varchar(255) DEFAULT NULL,
  `my_area` varchar(255) DEFAULT NULL,
  `pic0` varchar(255) DEFAULT NULL,
  `pic1` varchar(255) DEFAULT NULL,
  `pic2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of repair
-- ----------------------------
INSERT INTO `repair` VALUES ('10', '3CXP4pLhqe6W3yy4', '电梯故障', '公共区域', 'p_e00b08cdac807a9646837eea5eba3ab0854699da57f5d795.jpg', null, null);
INSERT INTO `repair` VALUES ('11', 'APdA59ufH053qPTa', '公共电梯故障', '公共区域', 'p_1a46e14bf80b2d6191863a11731a88764fe233cafa3b52bb.jpg', null, null);

-- ----------------------------
-- Table structure for survey
-- ----------------------------
DROP TABLE IF EXISTS `survey`;
CREATE TABLE `survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '调查标题',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of survey
-- ----------------------------
INSERT INTO `survey` VALUES ('17', '你的生活质量？');
INSERT INTO `survey` VALUES ('18', '你健康吗');

-- ----------------------------
-- Table structure for sur_que
-- ----------------------------
DROP TABLE IF EXISTS `sur_que`;
CREATE TABLE `sur_que` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `su_id` int(11) DEFAULT NULL,
  `qid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sur_que
-- ----------------------------
INSERT INTO `sur_que` VALUES ('19', '17', '14');
INSERT INTO `sur_que` VALUES ('20', '17', '15');
INSERT INTO `sur_que` VALUES ('21', '19', '16');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `buildingNumber` varchar(255) DEFAULT NULL,
  `unitNumber` varchar(255) DEFAULT NULL,
  `roomNumber` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `roomId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('Hv2T5VN61jW17Iq3', '小明', '男', '02', '01', '101', '18676221615', '123456', '0201101');

-- ----------------------------
-- Table structure for user_sur
-- ----------------------------
DROP TABLE IF EXISTS `user_sur`;
CREATE TABLE `user_sur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `qid` int(11) DEFAULT NULL,
  `su_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user_sur
-- ----------------------------
INSERT INTO `user_sur` VALUES ('49', 'NLAHQNt4607V594q', '14', '17');
INSERT INTO `user_sur` VALUES ('50', 'NLAHQNt4607V594q', '15', '17');
INSERT INTO `user_sur` VALUES ('51', '7MD1OoNHcT3l10au', '14', '17');
INSERT INTO `user_sur` VALUES ('52', '7MD1OoNHcT3l10au', '15', '17');
INSERT INTO `user_sur` VALUES ('53', 'Hdp8x0824y1A8U0O', '14', '17');
INSERT INTO `user_sur` VALUES ('54', 'Hdp8x0824y1A8U0O', '15', '17');
INSERT INTO `user_sur` VALUES ('55', '3CXP4pLhqe6W3yy4', '14', '17');
INSERT INTO `user_sur` VALUES ('56', '3CXP4pLhqe6W3yy4', '15', '17');
INSERT INTO `user_sur` VALUES ('57', 'APdA59ufH053qPTa', '14', '17');
INSERT INTO `user_sur` VALUES ('58', 'APdA59ufH053qPTa', '15', '17');
INSERT INTO `user_sur` VALUES ('59', 'Hv2T5VN61jW17Iq3', '14', '17');
INSERT INTO `user_sur` VALUES ('60', 'Hv2T5VN61jW17Iq3', '15', '17');

-- ----------------------------
-- Table structure for user_sur_opt
-- ----------------------------
DROP TABLE IF EXISTS `user_sur_opt`;
CREATE TABLE `user_sur_opt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_sur_id` int(11) DEFAULT NULL,
  `opt_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user_sur_opt
-- ----------------------------
INSERT INTO `user_sur_opt` VALUES ('45', '49', '35');
INSERT INTO `user_sur_opt` VALUES ('46', '50', '38');
INSERT INTO `user_sur_opt` VALUES ('47', '51', '33');
INSERT INTO `user_sur_opt` VALUES ('48', '52', '37');
INSERT INTO `user_sur_opt` VALUES ('49', '53', '34');
INSERT INTO `user_sur_opt` VALUES ('50', '54', '38');
INSERT INTO `user_sur_opt` VALUES ('51', '55', '36');
INSERT INTO `user_sur_opt` VALUES ('52', '56', '39');
INSERT INTO `user_sur_opt` VALUES ('53', '57', '33');
INSERT INTO `user_sur_opt` VALUES ('54', '58', '37');
INSERT INTO `user_sur_opt` VALUES ('55', '59', '33');
INSERT INTO `user_sur_opt` VALUES ('56', '60', '38');
